import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, TextInput, SafeAreaView, TouchableOpacity,FlatList, Dimensions } from "react-native";
import ListaChats from './ListaChats';
import ListaActivos from './ListaActivos';
import { Ionicons } from '@expo/vector-icons';
import { useFonts,
  Prompt_200ExtraLight,
  Prompt_300Light,
  Prompt_400Regular,
  Prompt_500Medium,
  Prompt_600SemiBold,
  Prompt_700Bold,
  Prompt_800ExtraBold,
  Prompt_900Black,
} from '@expo-google-fonts/prompt'

const Chats = ({navigation}) => {

  let [fontsLoaded] = useFonts({
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
    });


    return (

        <View style= {styles.container}>
            <View style = {styles.header}>
            <View style = {styles.izq}>
            <Ionicons name="arrow-back" size={30} color="#0435F0" />
            <Text style = {styles.titulo}>Chats</Text>
            </View>
                <Image
                style={{ 
                    width: 24,
                    height: 24,
                    marginTop: 21,
                    marginRight: 24,
                    alignSelf: 'end',
                    
                }}
                source={require("../../assets/image/buscador.png")}
                />
            </View>

            <View style={styles.perfiles}>
                <Text style={styles.activos}>Activos</Text>
                <ListaActivos />
            </View>

            <View style={styles.perfilUsuarios}>
                <Text style={styles.recientes}>Recientes</Text>
                <ListaChats />
            </View>

              
            </View>
    )}

   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },

      header: {
        flexDirection: 'row',
		    justifyContent: 'space-between',
      },

      izq :{
        flexDirection: 'row',
        marginTop: 29,
        marginLeft: 25,
      },

      titulo: {
        fontFamily: ' Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
      },

      perfiles: {
        marginLeft: 35,
      },

      activos: {
        fontFamily: ' Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginTop: 30,
       
      },

      perfilUsuarios: {
        marginLeft: 35,
      },

      recientes: {
        fontFamily: ' Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginTop: 22,
      }

    
    })
    


 export default Chats;