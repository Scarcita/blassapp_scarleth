import React from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import Gallery from './Gallery'
import { Ionicons } from '@expo/vector-icons';
import { useFonts,
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
  } from '@expo-google-fonts/prompt'

function NuevaCausa() {
    let [fontsLoaded] = useFonts({
        Prompt_200ExtraLight,
        Prompt_300Light,
        Prompt_400Regular,
        Prompt_500Medium,
        Prompt_600SemiBold,
        Prompt_700Bold,
        Prompt_800ExtraBold,
        Prompt_900Black,
        });

    const [descripcion, onChangeDescripcion] = React.useState("");
    const [monto, onChangeMonto] = React.useState("");
    const [fecha, onChangeFecha] = React.useState("");
    return (
        <View style={styles.container}>

            <View style= {styles.header}>
            <TouchableOpacity onPress={"MostrarCarrusel"}>
            <Ionicons name="arrow-back" size={30} color="#0435F0" /></TouchableOpacity>
            <Text style = {styles.titulo}>Nueva Causa</Text>
            </View>

            <View>
                <Gallery/>
            </View>

            <View>
                <Text style = {styles.descripcion}>Descripcion de la causa</Text>
            
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeDescripcion}
                    value={descripcion}
                    keyboardType="string"
                    placeholder='Descripcion...'   
                />
            </View>

            
            <View>
                <Text style = {styles.descripcion}>Monto de la donacion</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeMonto}
                    value={monto}
                    keyboardType="numeric"
                    placeholder='0'   
                />
            </View>

            
            <View>
                <Text style = {styles.descripcion}>Fecha de expiracion</Text>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeFecha}
                    value={fecha}
                    keyboardType="numeric"
                    placeholder='15/12/2020'   
                />
            </View>

            <View>
                <Text style = {styles.descripcion}>Datos de los receptores</Text>
            </View>



            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("MetodoPago")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                marginTop: 51,
                width: 324,
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt_600SemiBold',
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>

        </View>

    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',  
    },
    
    header: {
        flexDirection: 'row',
        marginTop: 29,
        marginLeft: 25,
      },

      titulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginBottom: 29
      },

    input: {
        width: 340,
        height: 44,
        borderWidth: 1,
        borderColor: '#0435F0',
        alignSelf: 'center',
        borderRadius: 6,
        //marginTop: 25,
        color: '#fff',
        fontFamily: 'Prompt_400Regular',
        fontSize: 18,
        paddingLeft: 10,
        marginBottom: 6,
        color: '#000000',
      },

      descripcion: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 20,
        color: '#000000',
        marginBottom: 3,
        marginLeft: 25
        
      }
})

export default NuevaCausa;

