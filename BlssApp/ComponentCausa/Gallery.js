import React, { useState, useEffect } from 'react';
import * as ImagePicker from 'expo-image-picker';
import { Button, Image, View, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';


export default function GalleryComponenet() {
	const [image1, setImage1] = useState(null);
	const [image2, setImage2] = useState(null);
	const [image3, setImage3] = useState(null);
	const [image4, setImage4] = useState(null);
	
	useEffect(() => {
		(async () => {
		if (Platform.OS !== 'web') {
			const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
			if (status !== 'granted') {
			alert('Sorry, Camera roll permissions are required to make this work!');
			}
		}
		})();
	}, []);
	
	const chooseImg = async ({numero}) => {
		let result = await ImagePicker.launchImageLibraryAsync({
			mediaTypes: ImagePicker.MediaTypeOptions.All,
			aspect: [4, 3],
			quality: 1,			
			allowsEditing: true,
		});
	
		console.log(result);

		switch (numero) {
			case 1:
				if (!result.cancelled) {
					setImage1(result.uri);
				}
				
				break;

			case 2:
				if (!result.cancelled) {
					setImage2(result.uri);
				}
				break;

			case 3:
				if (!result.cancelled) {
					setImage3(result.uri);
				}
				break;

			case 4:
				if (!result.cancelled) {
					setImage4(result.uri);
				}
				break;
		
			default:
				break;
		}



	};
	
	return (
		<View>
			<TouchableOpacity onPress={chooseImg("1")}>
				<View style={styles.cargarImagen}>
				{image1 && <Image source={{ uri: image1 }} style={{ width: 342, height: 134, borderRadius: 11 }} />}	
				</View>
			</TouchableOpacity>

			<View style={styles.alinear}>
			
			<TouchableOpacity onPress={chooseImg(2)}>
			<View style={styles.cargarImagen1}>
            {image2 && <Image source={{ uri: image2 }} style={{ width: 107, height: 107, borderRadius: 11 }} />}	
		    </View>
			</TouchableOpacity>

			<TouchableOpacity onPress={chooseImg(3)}>
			<View style={styles.cargarImagen1}>
            {image3 && <Image source={{ uri: image3 }} style={{ width: 107, height: 107, borderRadius: 11 }} />}	
		    </View>
			</TouchableOpacity>

			<TouchableOpacity onPress={chooseImg(4)}>
			<View style={styles.cargarImagen1}>
            {image4 && <Image source={{ uri: image3 }} style={{ width: 107, height: 107, borderRadius: 11}} />}	
		    </View>
			</TouchableOpacity>

			</View>
        
		</View>
		
	);
}

const styles = StyleSheet.create({
    
    cargarImagen: {
        backgroundColor: '#D9D9D9',
		width: 342,
		height: 134,
		alignSelf: 'center',
		marginBottom: 14,
		borderRadius: 11,
      },

	  alinear: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginLeft: 25,
		marginRight: 25

	  },

	  cargarImagen1: {
        backgroundColor: '#D9D9D9',
		width: 107,
		height: 107,
		marginBottom: 18,
		borderRadius: 11
		
      },
})