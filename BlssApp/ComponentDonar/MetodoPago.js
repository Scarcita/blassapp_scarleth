import React, { useState } from 'react';
import {  View,Text,Image, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import RadioButton from '../ComponentDonar/RadioButton'
import { ComponentCheck } from './ComponentCheck'
import { Ionicons } from '@expo/vector-icons';

const MetodoPago = ({navigation}) => {

    const [isSelected, setSelection] = useState(false);

    const PROP = [
        {
         key: '1',
         img: "../../assets/image/Vector.png",
         name: 'My Stripe'
        },
     
        {
         key: '2',
         img: "../../assets/image/mastercard.png",
         name: 'MasterCard'
        },
        {
         key: '3',
         img: "../../assets/image/paypal.png",
         name: 'PayPal'
        },
        {
         key: '4',
         img: "../../assets/image/visa.png",
         name: 'Visa'
        },
     
     ]
     const optionsindividual = [{text: 'Donante anonimo', id: 1}]

    return (

        <View style= {styles.container}>
            <View style= {styles.header}>
            <TouchableOpacity onPress={"MostrarCarrusel"}>
            <Ionicons name="arrow-back" size={30} color="#0435F0" /></TouchableOpacity>
            <Text style = {styles.titulo}>Checkout</Text>
            </View>
            <Text style = {styles.subTitulo}>Metodo de pago</Text>

            <View>
                <RadioButton PROP={PROP}/>
            </View>

            {/* <SafeAreaView style= {styles.checkboxcontainer}>
                <ComponentCheck
                options={optionsindividual} onChange={op => alert(op)}/>
                <Text style={styles.descripcion}>Donate is anonymous</Text>

            </SafeAreaView> */}


            <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("Successful")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                width: 324,
                height: 38,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt_600SemiBold',
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>

        </View>

            
    )
}
            
   
    
const styles = StyleSheet.create ({
    container: {
      flex: 1, 
      backgroundColor: '#fff',   
      },
      header: {
        flexDirection: 'row',
        marginTop: 29,
        marginLeft: 35,
      },

      titulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 22,
        color: '#000000',
        marginBottom: 29
      },
      subTitulo: {
        fontFamily: 'Prompt_600SemiBold',
        fontSize: 20,
        color: '#000000',
        marginLeft: 48,
        marginBottom: 14
        
      },

      checkboxcontainer: {
        flexDirection: 'row',
      },

      checkbox: {
        width: 19,
        height: 19,
        marginLeft: 50,
        borderRadius: 1,
        borderColor: '#0435F0',
        marginBottom: 177,
      },

      descripcion: {
        fontFamily: 'Prompt_400Regular',
        fontSize: 13,
        color: '#000000',
        marginLeft: 8

      }

      
    })
    


 export default MetodoPago;