export const imageData =
    [{
        title:'Nombre de causa 1',
        url: 'https://cdn.businessinsider.es/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2019/06/importancia-ensenar-ninos-donar-dinero.jpg?itok=aFwCMGKw',
        id:1
    },

    {
        title:'Nombre de causa 2',
        url: 'https://www.bbva.com/wp-content/uploads/2016/10/crowdfunding1.jpg',
        id:2
    },

    {
        title:'Nombre de causa 3',
        url: 'https://www.consumer.es/app/uploads/2010/11/donacion.jpg',
        id:3
    }]

    export default imageData;