import React, { useState } from 'react';
import {  View,Text, StyleSheet, SafeAreaView, TouchableOpacity,FlatList, Dimensions } from "react-native";
import Carrusel from '../BlssApp/Carrusel'
import imageData from './Data/Data'
import ListaUsuarios from './ListaUsuarios';
import * as Progress from 'react-native-progress'
import SingleUser from './SingleUser';

import { useFonts,
    Prompt_200ExtraLight,
    Prompt_300Light,
    Prompt_400Regular,
    Prompt_500Medium,
    Prompt_600SemiBold,
    Prompt_700Bold,
    Prompt_800ExtraBold,
    Prompt_900Black,
  } from '@expo-google-fonts/prompt'


const MostrarCarrusel = ({navigation}) => {
    const[range, setRange] = useState ('10')
    const [Sliding, setSliding] = useState("100")

    let [fontsLoaded] = useFonts({
        Prompt_200ExtraLight,
        Prompt_300Light,
        Prompt_400Regular,
        Prompt_500Medium,
        Prompt_600SemiBold,
        Prompt_700Bold,
        Prompt_800ExtraBold,
        Prompt_900Black,
        });
    
    return (
        <View style={styles.container}>
            <Carrusel data = {imageData}/>
            <View>
                <Text style={{marginLeft: 26, marginRight: 26, fontSize: 12, fontFamily: 'Prompt_300Light', marginBottom: 5,}}>Mostramos el diseno que tenemos del porcentaje completado de la causa, una foto destacada y la descripcion de la misma</Text>
                <View style={styles.infMonto}>
                    <Text style={{fontSize:14, color: "#0435F0", marginLeft: 26, fontFamily: 'Prompt_400Regular',
                    }}>${range} raised from $2000</Text>
                    <Text style={{fontSize:14, color: "#0435F0", marginLeft: 26, fontFamily: 'Prompt_400Regular',
                     textAlign: 'end', marginRight: 26 }}>17 Dias left</Text>
                </View>

                <SafeAreaView>

                    <View style={{marginLeft: 26, marginRight: 26}}>
                        <Progress.Bar progress={0.5} width= {360} height={6} backgroundColor='#0435F0' color='#0435F0' ></Progress.Bar>
                    </View>

                </SafeAreaView>         
        
        </View>
        <View style={styles.perfiles}>
            <ListaUsuarios />
            <Text style={styles.donacion}>from $3456.08</Text>
        </View>
        
        <TouchableOpacity
// BOTON DE DONAR
        onPress={() => navigation.navigate("Donacion")}
            style={{
                backgroundColor: "#0435F0",
                paddingTop: 5,
                paddingBottom: 5,
                //marginTop: '10%',
                width: '90%',
                height: 38,
                marginLeft: 26,
                marginRight: 26,
                alignSelf: 'center',
                borderRadius: 6,
            }}
        >
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Prompt_600SemiBold',
                }}
            >
                Donar Ahora
            </Text>
        </TouchableOpacity>
        
        <View style={styles.donantes}>
            <SingleUser />
        </View>
        </View>
        
        
    )
}

const styles = StyleSheet.create ({
    container: {
      flex: 1,
      backgroundColor: '#fff', 
         
      },

       perfiles: {
        marginLeft: 26,
        marginTop: 6,
        display: 'grid',
        gridTemplateColumns: '60% 40%', 
      },

      donacion: {
        fontFamily:  'Prompt_600SemiBold',
        fontSize: 18,
        color: '#0435F0'
      },
      
      donantes: {
        
        
      }
    })

 export default MostrarCarrusel;