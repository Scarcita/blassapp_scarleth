import React from "react";
import { View,Text, StyleSheet, Alert, TouchableOpacity } from "react-native";
import PagarPlan from "./PagarPlan"


function ComprarPlan() {
  return (
    <View style={styles.container}>
      <View>
      <Text style={styles.titulo}>Sesiones</Text>
      <Text style={styles.descripcion}>Plan Mensual 30 Sesiones</Text>

      <PagarPlan plan='Plan Mensual' sesiones='118' precio='Bs 349'/>
      </View>
    </View>

  ) 
}

export default ComprarPlan;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#1C1B1B', 
  },

  titulo: {
    color: '#fff',
    fontSize: '51pt',
    marginLeft: '24pt',
    marginTop: '27pt',
    //fontFamily: 'Dosis-Regular'
  },

  descripcion: {
    color: '#FFF843',
    fontSize: '17pt',
    marginLeft: '24pt',
    //fontFamily: 'Dosis-Regular'
  },

  

});