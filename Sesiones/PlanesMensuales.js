import React from "react";
import { View,Text, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from '@react-navigation/native';


const PlanesMensuales = ({planItem}) =>  {
   const navigation = useNavigation();
  
   const mostrarPlan = () => {
    navigation.navigate("ComprarPlan");
  }

  const {plan, sesiones, precio} = planItem

  return (
    <View style={styles.container}>

      <View style={styles.informacion}>
        <TouchableOpacity
            onPress={mostrarPlan}  >
            <Text style={styles.text1}>{plan}</Text>
            <Text style={styles.text2}>{sesiones}</Text>
            <Text style={styles.text3}>{precio}</Text>

        </TouchableOpacity> 

      </View>

      
    </View>

    
  )
    
};

export default PlanesMensuales;

const styles = StyleSheet.create ({
    container: {
      flex: 1,
      backgroundColor: '#1C1B1B',
      justifyContent: 'center',
      //textAlign: 'center',
      alignItems: 'center',
       
      
      
    },
  
    informacion: {
      width: '207pt',
      height: '132pt',
      borderWidth: '1pt',
      borderColor: '#FFF843',
      borderRadius: '21pt',
      //marginLeft: '84pt',
      //marginRight: '84pt',
      marginTop: '59pt',
      
    },
  
    text1: {
      color: '#FFF843',
      fontSize: '12pt',
      marginLeft: '9pt',
      marginTop: '17pt',
      
    },
  
    text2: {
      color: '#FFF843',
      fontSize: '10pt',
      marginLeft: '9pt'
      
    },
  
    text3: {
      color: '#FFF843',
      fontSize: '47pt',
      marginLeft: '9pt'
    
    },
  
  
  });