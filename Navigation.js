import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer} from "@react-navigation/native";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native-web";

///navegacion

import HomeScreen from "./screens/HomeScreen";
import RutinasScreen from "./screens/RutinasScreen";
import SesionesScreen from "./screens/SesionesScreen";
import PlanMensual from "./Sesiones/PlanMensual";
import ComprarPlan from "./Sesiones/ComprarPlan"
import ListaMedidas from "./Medidas/ListaMedidas";

//BlssApp

import MostrarCarrusel from "./BlssApp/MostrarCarrusel";
import Donacion from "./BlssApp/ComponentDonar/Donacion";
import MetodoPago from "./BlssApp/ComponentDonar/MetodoPago";
import Successful  from "./BlssApp/ComponentDonar/Successful";
import Chats from "./BlssApp/ComponentChats/Chats";
import NuevaCausa from './BlssApp/ComponentCausa/NuevaCausa'



//import MaterialComunityIcons from 'react-native-vector-icons/MaterialComunityIcons';

const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator
    initialRouteName="Home"
      screenOptions={{
        tabBarShowLabel: false,
        style: {
          position: 'absolute',
          elevation: 0,
          color: '#fff',
          backgroundColor: '#000000',
          with: '375pt',
          height: '200pt',
          
      }
    }}
    >
      
      <Tab.Screen 
      name="Home" 
      component={HomeScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
          >
          <Image
          source={require('./assets/img/trazado.png')}
          resizeMode="contain"
          style={{
            with: '19.8pt',
            height: '19.8pt',
            tintColor: focused? '#FFF843' : '#000000'
            
          }}/>
          <Text
          style={{color: focused ? '#FFF843' : '#000000', fontSize: 10}}>Home </Text>
          </View>
        )   
      }}/>

      <Tab.Screen 
      name="Rutinas" 
      component={RutinasScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
          >
          <Image
          source={require('./assets/img/gym.png')}
          resizeMode="contain"
          style={{
            with: '19.8pt',
            height: '19.8pt',
            tintColor: focused? '#FFF843' : '#000000'
            
          }}/>
          <Text
          style={{color: focused ? '#FFF843' : '#000000', fontSize: 10}}>Rutinas </Text>
          </View>
        )
        
        
      }} />

      <Tab.Screen 
      name="Sesiones" 
      component={SesionesScreen}
      options={{
        tabBarIcon: ({ focused }) => (
          <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
          >
          <Image
          source={require('./assets/img/brazo.png')}
          resizeMode="contain"
          style={{
            with: '19.8pt',
            height: '19.8pt',
            tintColor: focused? '#FFF843' : '#000000'
            
          }}/>
          <Text
          style={{color: focused ? '#FFF843' : '#000000', fontSize: 10}}>Sesiones</Text>
          </View>
        )  
      }}/>

    <Tab.Screen 
      name="PlanMensual" 
      component={PlanMensual}
      />

    <Tab.Screen 
      name="ComprarPlan" 
      component={ComprarPlan}
      />

    <Tab.Screen 
      name="ListaMedidas" 
      component={ListaMedidas}
      />

    <Tab.Screen 
      name="MostrarCarrusel" 
      component={MostrarCarrusel}
      options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
      name="Donacion" 
      component={Donacion}
      options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
      name="MetodoPago" 
      component={MetodoPago}
      options={{
        headerShown: false 
      }}
      />
      <Tab.Screen 
      name="Successful" 
      component={Successful}
      options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
      name="Chats" 
      component={Chats}
      options={{
        headerShown: false 
      }}
      />

    <Tab.Screen 
      name="NuevaCausa" 
      component={NuevaCausa}
      options={{
        headerShown: false 
      }}
    />

      
    </Tab.Navigator>

    
    
    );  
    
 
}

export default MyTabs;

