import React from "react";
import { View,Text, StyleSheet, TouchableOpacity, SafeAreaView, FlatList } from "react-native";
import SesionesProps from "../Sesiones/SesionesProps";


const SesionesScreen = ({navigation}) => {

  const listaSesiones = [
    {
      id: '1',
      fecha: '24/10/2021',
      plan: '30 Sesiones',
      precio: 'Bs. 349',
      sesion: '10',
    },

    {
      id: '2',
      fecha: '24/10/2021',
      plan: '58 Sesiones',
      precio: 'Bs. 349',
      sesion: '10',
    },

    {
      id: '3',
      fecha: '24/10/2021',
      plan: '110 Sesiones',
      precio: 'Bs. 349',
      sesion: '10',
    },


]
  return ( 
    <SafeAreaView style={{
      backgroundColor: '#1C1B1B',
      width: '100%',
      height: '100%'
    }}>
      <View>
        <Text style={styles.titulo}>Sesiones</Text>
      </View>

      <TouchableOpacity
       onPress={()=> navigation.navigate("PlanMensual")}>
        <View>
          <Text style={styles.comprar}>Comprar sesiones</Text>  
        </View>
      

      <FlatList
      data = {listaSesiones}
      keyExtractor = {(item) => item.id}
      renderItem = {({item, index}) => <SesionesProps item = {item}/>}
      />
      </TouchableOpacity> 
    </SafeAreaView> 
    
  )
    
}

export default SesionesScreen;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#1C1B1B',
    
    
  },
  titulo: {
    color: '#fff',
    fontSize: '51pt',
    marginLeft: '24pt',
    marginTop: '27pt',
    
   
    //fontFamily: 'Dosis-Regular'
  },

  comprar: {
    color: '#FFF843',
    textAlign: 'end',
    marginRight: '40pt',
    marginTop: '19pt',
    marginBottom: '18pt'
  },

});