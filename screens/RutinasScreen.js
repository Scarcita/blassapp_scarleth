import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, StatusBar, ActivityIndicator, FlatList } from "react-native";
import PlanMensual from "../Sesiones/PlanMensual";
import PlanesMensuales from "../Sesiones/PlanesMensuales";



const RutinasScreen = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);

  return (
    <View style={{ flex: 1, padding: 24}}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
        data = {data}
        keyExtractor= {({id}) => id}
        renderItem={({item})=> (
          <View>
            <Text>{item.avatar} </Text>
            <Image  style={{ 
                width: '22pt',
                height: '22pt',
                //marginTop: '60pt',
                //marginLeft: '36pt', 
                border: '2px solid #FDFDFD',
                marginBottom: 15,
                borderRadius: '50%'
              }} 
              source={(item.avatar)} ></Image>
            </View>
        )} ></FlatList>
        
      ) }
    </View>
  );
    
}

export default RutinasScreen;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000000',
    color: 'fff'
  },
});